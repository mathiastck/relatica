import 'package:flutter_test/flutter_test.dart';
import 'package:relatica/friendica_client/paging_data.dart';

void main() {
  group('From Query Parameters', () {
    test('No query string', () {
      final paging = PagingData.fromQueryParameters(
        Uri.parse('https://localhost'),
      );
      expect(paging, equals(PagingData()));
    });
    test('All Terms', () {
      final paging = PagingData.fromQueryParameters(
        Uri.parse(
            'https://localhost?&limit=49&max_id=48&min_id=46&since_id=47'),
      );
      expect(paging,
          equals(PagingData(maxId: 48, sinceId: 47, minId: 46, limit: 49)));
    });
  });

  group('To query parameters', () {
    test('Default', () {
      expect(
        PagingData().toQueryParameters(),
        equals('limit=50'),
      );
    });

    test('Specific limit only', () {
      expect(
        PagingData(limit: 10).toQueryParameters(),
        equals('limit=10'),
      );
    });

    test('MinID only', () {
      expect(
        PagingData(maxId: 10).toQueryParameters(),
        equals('limit=50&min_id=10'),
      );
    });

    test('MaxID only', () {
      expect(
        PagingData(maxId: 10).toQueryParameters(),
        equals('limit=50&max_id=10'),
      );
    });

    test('SinceID only', () {
      expect(
        PagingData(sinceId: 10).toQueryParameters(),
        equals('limit=50&since_id=10'),
      );
    });

    test('All Defined', () {
      expect(
        PagingData(
          minId: 9,
          sinceId: 10,
          maxId: 11,
          limit: 12,
        ).toQueryParameters(),
        equals('limit=12&min_id=9&since_id=10&max_id=11'),
      );
    });
  });
}
