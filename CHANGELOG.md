# Relatica Change Log

## Version 0.x.0 (beta)

* Fixed image viewer quirks with text overwriting, lack of mouse scrollwheel zoom, and
  responsiveness
  quirk (https://gitlab.com/mysocialportal/relatica/-/merge_requests/10)

## Version 0.1.0 (beta)

* Initial public release, as an early beta.
* Major working features (at least in initial implementation versions):
    * Logging in with username/password. These are stored using the OS specific key vaults
    * Writing posts
        * Typing @ brings up a list of all known fediverse accounts that the app has ever seen as
          you type (but not all
          that your server has seen)
        * Typing # brings up a list of all known hashtags that the app has ever seen as you type (
          but not all that your
          server has seen)
        * Very basic markdown (wrapping in stars for bold/italics)
        * Adding new images to posts
            * Image posting with easy means of adding ALT text
        * Attaching existing images to posts
    * Resharing posts
    * Browsing timelines including from groups you've created. In this app "Home" is the same as
      the "Network" (grid)
      button in Frio and "Yours" is the equivalent of the Home button in the app.
    * Browsing notifications and marking individual ones read or all read
    * Getting gallery list and browsing galleries
    * Browsing your contacts or searching contacts that are known by your app
    * Adjudicating follow requests
    * Going to individual profiles to see their posts, follow/unfollow, add/remove from groups
    * A "low bandwidth" mode that defers loading of images until you click on them, unless the app
      already has that
      image cached locally.
    * Light/dark mode selection
    * Opening posts, comments, and profiles in the "external" browser
    * Playing videos within the app on iOS and Android.
    * Pulling down to refresh timelines, galleries, and posts but loading newer or older posts has
      specific buttons (
      this will probably change in the near future)
    * Pulling down to refresh notifications and contacts gets updates to that respective data (this
      may change in the
      near future)

## Version 0.1.0b2 (beta)

* Fixes
    * Scrollwheel zooming now works correctly on desktop
    * Fix image overflow and apparent intermittent lack of zoom responsiveness in image preview pane
    * Fix inconsistent tap behavior on notifications cards
    * Search on the contacts screen now searches handles as well as names
    * All contacts now load properly from server
* New Features
    * Initial paging infrastructure for being able to incrementally load larger sets of things (like
      comments)
    * First usage of paging system: contacts
    * A "Clear Caches" button that clears all memory and disk data. Useful if you want to do a reset
      on things like tags, contacts, posts timelines, etc.
    * Initial implementation of direct messaging including:
        * List of conversation threads
        * Reading conversation threads
        * Marking conversations read by tapping on the message
        * Respond in a conversation
        * Start a new conversation by searching for a known contact "type @ and start typing like in
          posts/comments"
