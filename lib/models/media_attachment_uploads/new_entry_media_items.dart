import 'media_upload_attachment.dart';

class NewEntryMediaItems {
  String albumName;

  final List<MediaUploadAttachment> attachments;

  NewEntryMediaItems({
    this.albumName = '',
    List<MediaUploadAttachment>? existingAttachments,
  }) : attachments = existingAttachments ?? [];

  @override
  String toString() {
    return 'EntryMediaItems{albumName: $albumName, attachments: $attachments}';
  }
}
