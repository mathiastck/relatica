enum TimelineType {
  home,
  global,
  local,
  group,
  profile,
  self;

  String toLabel() {
    switch (this) {
      case TimelineType.home:
        return 'Home';
      case TimelineType.global:
        return 'Global';
      case TimelineType.local:
        return 'Local';
      case TimelineType.group:
        return 'Group';
      case TimelineType.profile:
        return 'Profile';
      case TimelineType.self:
        return 'Yours';
    }
  }
}

class TimelineIdentifiers {
  final TimelineType timeline;
  final String auxData;

  TimelineIdentifiers({required this.timeline, this.auxData = ''});

  String toHumanKey() {
    return auxData.isEmpty ? timeline.name : '${timeline.name}_$auxData';
  }

  factory TimelineIdentifiers.home() =>
      TimelineIdentifiers(timeline: TimelineType.home);

  factory TimelineIdentifiers.profile(String profileId) => TimelineIdentifiers(
        timeline: TimelineType.profile,
        auxData: profileId,
      );

  @override
  String toString() {
    return auxData.isEmpty
        ? 'TimelineIdentifiers{timeline: $timeline)'
        : 'TimelineIdentifiers{timeline: $timeline, auxData: $auxData}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimelineIdentifiers &&
          runtimeType == other.runtimeType &&
          timeline == other.timeline &&
          auxData == other.auxData;

  @override
  int get hashCode => timeline.hashCode ^ auxData.hashCode;
}
