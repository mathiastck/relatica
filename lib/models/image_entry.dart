class ImageEntry {
  final String id;
  final String album;
  final String filename;
  final String description;
  final String thumbnailUrl;
  final DateTime created;
  final int height;
  final int width;
  final List<ImageEntryScale> scales;

  ImageEntry(
      {required this.id,
      required this.album,
      required this.filename,
      required this.description,
      required this.thumbnailUrl,
      required this.created,
      required this.height,
      required this.width,
      required this.scales});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ImageEntry && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}

class ImageEntryScale {
  final String id;
  final int scale;
  final Uri link;
  final int width;
  final int height;
  final int size;

  ImageEntryScale({
    required this.id,
    required this.scale,
    required this.link,
    required this.width,
    required this.height,
    required this.size,
  });
}
