import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../globals.dart';
import '../../models/image_entry.dart';
import '../../screens/existing_image_selector_screen.dart';
import '../../screens/image_viewer_screen.dart';
import '../../serializers/friendica/image_entry_friendica_extensions.dart';
import '../padding.dart';

class GallerySelectorControl extends StatefulWidget {
  final List<ImageEntry> entries;

  const GallerySelectorControl({super.key, required this.entries});

  @override
  State<GallerySelectorControl> createState() => _GallerySelectorControlState();
}

class _GallerySelectorControlState extends State<GallerySelectorControl> {
  @override
  Widget build(BuildContext context) {
    const thumbnailSize = 50.0;
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(
          'Existing Images',
          style: Theme.of(context).textTheme.titleLarge,
        ),
        IconButton(
          onPressed: () async {
            final result = await Navigator.of(context).push<List<ImageEntry>>(
                MaterialPageRoute(
                    builder: (context) => ExistingImageSelectorScreen()));
            if (result != null) {
              setState(() {
                widget.entries.clear();
                widget.entries.addAll(result);
              });
            }
          },
          icon: const Icon(Icons.photo_library),
        ),
      ]),
      const VerticalDivider(),
      if (widget.entries.isNotEmpty)
        SizedBox(
          height: thumbnailSize,
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            separatorBuilder: (context, index) => const HorizontalPadding(
              width: 5,
            ),
            itemBuilder: (context, index) {
              final item = widget.entries[index];
              return GestureDetector(
                onDoubleTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ImageViewerScreen(
                        attachment: item.toMediaAttachment(),
                      ),
                    ),
                  );
                },
                onTap: () {
                  setState(() async {
                    final confirm = await showYesNoDialog(
                        context, 'Remove image from post?');
                    if (confirm == true) {
                      widget.entries.remove(item);
                    }
                  });
                },
                child: CachedNetworkImage(
                  width: thumbnailSize,
                  height: thumbnailSize,
                  imageUrl: item.thumbnailUrl,
                ),
              );
            },
            itemCount: widget.entries.length,
          ),
        ),
    ]);
  }
}
