import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import '../services/setting_service.dart';

final _shownVideos = <String>{};

class VideoControl extends StatefulWidget {
  final String videoUrl;
  final double width;
  final double height;

  const VideoControl({
    super.key,
    required this.videoUrl,
    required this.width,
    required this.height,
  });

  @override
  State<VideoControl> createState() => _VideoControlState();
}

class _VideoControlState extends State<VideoControl> {
  late final VideoPlayerController videoPlayerController;

  @override
  void initState() {
    videoPlayerController = VideoPlayerController.network(widget.videoUrl)
      ..initialize().then((_) {
        setState(() {});
      });
  }

  @override
  void dispose() {
    super.dispose();
    videoPlayerController.dispose();
  }

  @override
  void deactivate() {
    videoPlayerController.pause();
    super.deactivate();
  }

  void showVideo() {
    _shownVideos.add(widget.videoUrl);
    setState(() {});
  }

  void toggleVideoPlay() {
    videoPlayerController.value.isPlaying
        ? videoPlayerController.pause()
        : videoPlayerController.play();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final shown = !context.watch<SettingsService>().lowBandwidthMode ||
        _shownVideos.contains(widget.videoUrl);

    final placeHolderBox = SizedBox(
      width: widget.width,
      height: widget.height,
      child: const Card(
          color: Colors.black12,
          shape: RoundedRectangleBorder(),
          child: Icon(Icons.movie)),
    );

    if (!shown) {
      return GestureDetector(
        onTap: showVideo,
        child: placeHolderBox,
      );
    }

    _shownVideos.add(widget.videoUrl);
    if (!videoPlayerController.value.isInitialized) {
      return placeHolderBox;
    }
    final size = videoPlayerController.value.size;
    final videoWidth = size.width <= widget.width ? size.width : widget.width;
    final videoHeight = size.width <= widget.width
        ? size.height
        : size.height * videoWidth / size.width;
    return GestureDetector(
      onTap: toggleVideoPlay,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: videoWidth,
            height: videoHeight,
            child: AspectRatio(
              aspectRatio: videoPlayerController.value.aspectRatio,
              child: VideoPlayer(videoPlayerController),
            ),
          ),
          Row(
            children: [
              IconButton(
                icon: videoPlayerController.value.isPlaying
                    ? const Icon(Icons.pause)
                    : const Icon(Icons.play_arrow),
                onPressed: toggleVideoPlay,
              ),
              IconButton(
                  onPressed: () {
                    videoPlayerController.seekTo(Duration.zero);
                  },
                  icon: const Icon(Icons.replay)),
            ],
          )
        ],
      ),
    );
  }
}
