import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../../models/TimelineIdentifiers.dart';
import '../../services/timeline_manager.dart';
import 'post_control.dart';

class TimelinePanel extends StatelessWidget {
  static final _logger = Logger('$TimelinePanel');
  final TimelineIdentifiers timeline;

  const TimelinePanel({super.key, required this.timeline});

  @override
  Widget build(BuildContext context) {
    final manager = context.watch<TimelineManager>();
    final result = manager.getTimeline(timeline);
    if (result.isFailure) {
      return Center(child: Text('Error getting timeline: ${result.error}'));
    }
    final items = result.value;
    return RefreshIndicator(
      onRefresh: () async {
        await manager.updateTimeline(timeline, TimelineRefreshType.refresh);
      },
      child: ListView.separated(
        itemBuilder: (context, index) {
          if (index == 0) {
            return TextButton(
                onPressed: () async => await manager.updateTimeline(
                    timeline, TimelineRefreshType.loadNewer),
                child: const Text('Load newer posts'));
          }

          if (index == items.length + 1) {
            return TextButton(
                onPressed: () async => await manager.updateTimeline(
                    timeline, TimelineRefreshType.loadOlder),
                child: const Text('Load older posts'));
          }
          final itemIndex = index - 1;
          final item = items[itemIndex];
          _logger.finest(
              'Building item: $itemIndex: ${item.entry.toShortString()}');
          return PostControl(
            originalItem: item,
            scrollToId: item.id,
            openRemote: false,
            showStatusOpenButton: true,
            isRoot: false,
          );
        },
        separatorBuilder: (context, index) => Divider(),
        itemCount: items.length + 2,
      ),
    );
  }
}
