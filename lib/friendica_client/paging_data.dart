class PagingData {
  static const DEFAULT_LIMIT = 50;

  final int? minId;
  final int? maxId;
  final int? sinceId;
  final int limit;

  PagingData({
    this.minId,
    this.maxId,
    this.sinceId,
    this.limit = DEFAULT_LIMIT,
  });

  factory PagingData.fromQueryParameters(Uri uri) {
    final minIdString = uri.queryParameters['min_id'];
    final maxIdString = uri.queryParameters['max_id'];
    final sinceIdString = uri.queryParameters['since_id'];
    final limitString = uri.queryParameters['limit'];
    return PagingData(
      minId: int.tryParse(minIdString ?? ''),
      maxId: int.tryParse(maxIdString ?? ''),
      sinceId: int.tryParse(sinceIdString ?? ''),
      limit: int.tryParse(limitString ?? '') ?? DEFAULT_LIMIT,
    );
  }

  String toQueryParameters({String limitKeyword = 'limit'}) {
    var pagingData = '$limitKeyword=$limit';
    if (minId != null) {
      pagingData = '$pagingData&min_id=$minId';
    }

    if (sinceId != null) {
      pagingData = '$pagingData&since_id=$sinceId';
    }

    if (maxId != null) {
      pagingData = '$pagingData&max_id=$maxId';
    }

    return pagingData;
  }

  @override
  String toString() {
    return 'PagingData{maxId: $maxId, minId: $minId, sinceId: $sinceId, limit: $limit}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PagingData &&
          runtimeType == other.runtimeType &&
          minId == other.minId &&
          maxId == other.maxId &&
          sinceId == other.sinceId &&
          limit == other.limit;

  @override
  int get hashCode =>
      minId.hashCode ^ maxId.hashCode ^ sinceId.hashCode ^ limit.hashCode;
}
