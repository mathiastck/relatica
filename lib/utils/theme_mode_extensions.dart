import 'package:flutter/material.dart';

extension ThemeModeExtensions on ThemeMode {
  static ThemeMode parse(String? themeModeString) =>
      ThemeMode.values.firstWhere(
        (m) => m.name == themeModeString,
        orElse: () => ThemeMode.system,
      );

  String toLabel() {
    switch (this) {
      case ThemeMode.system:
        return 'System';
      case ThemeMode.light:
        return 'Light';
      case ThemeMode.dark:
        return 'Dark';
    }
  }
}
