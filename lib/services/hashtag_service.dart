import 'package:flutter/foundation.dart';

import '../data/interfaces/hashtag_repo_intf.dart';
import '../globals.dart';
import '../models/hashtag.dart';

class HashtagService extends ChangeNotifier {
  late final IHashtagRepo repo;

  HashtagService() {
    repo = getIt<IHashtagRepo>();
  }

  void add(Hashtag tag) {
    repo.add(tag);
    notifyListeners();
  }

  List<String> getMatchingHashTags(String searchString) {
    return repo.getMatchingHashTags(searchString);
  }
}
