import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData light = ThemeData(primarySwatch: Colors.indigo);

  static ThemeData dark = ThemeData.from(
      colorScheme: ColorScheme.fromSwatch(
    primarySwatch: Colors.cyan,
    brightness: Brightness.dark,
  ));
}
