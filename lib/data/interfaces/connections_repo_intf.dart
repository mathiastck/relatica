import 'package:result_monad/result_monad.dart';

import '../../models/connection.dart';
import '../../models/exec_error.dart';

class IConnectionsRepo {
  void clear() {
    throw UnimplementedError();
  }

  bool addConnection(Connection connection) {
    throw UnimplementedError();
  }

  bool addAllConnections(Iterable<Connection> newConnections) {
    throw UnimplementedError();
  }

  bool updateConnection(Connection connection) {
    throw UnimplementedError();
  }

  Result<Connection, ExecError> getById(String id) {
    throw UnimplementedError();
  }

  Result<Connection, ExecError> getByName(String name) {
    throw UnimplementedError();
  }

  Result<Connection, ExecError> getByHandle(String handle) {
    throw UnimplementedError();
  }

  List<Connection> getMyContacts() {
    throw UnimplementedError();
  }

  List<Connection> getKnownUsersByName(String name) {
    throw UnimplementedError();
  }
}
