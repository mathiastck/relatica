import 'package:go_router/go_router.dart';

import 'globals.dart';
import 'screens/contacts_screen.dart';
import 'screens/editor.dart';
import 'screens/follow_request_adjudication_screen.dart';
import 'screens/gallery_browsers_screen.dart';
import 'screens/gallery_screen.dart';
import 'screens/home.dart';
import 'screens/menus_screen.dart';
import 'screens/message_thread_screen.dart';
import 'screens/message_threads_browser_screen.dart';
import 'screens/messages_new_thread.dart';
import 'screens/notifications_screen.dart';
import 'screens/post_screen.dart';
import 'screens/profile_screen.dart';
import 'screens/settings_screen.dart';
import 'screens/sign_in.dart';
import 'screens/splash.dart';
import 'screens/user_posts_screen.dart';
import 'screens/user_profile_screen.dart';
import 'services/auth_service.dart';

class ScreenPaths {
  static String thread = '/thread';
  static String connectHandle = '/connect';
  static String contacts = '/contacts';
  static String splash = '/splash';
  static String menu = '/menu';
  static String settings = '/settings';
  static String messages = '/messages';
  static String timelines = '/';
  static String gallery = '/gallery';
  static String profile = '/profile';
  static String notifications = '/notifications';
  static String signin = '/signin';
  static String signup = '/signup';
  static String userProfile = '/user_profile';
  static String userPosts = '/user_posts';
}

bool needAuthChangeInitialized = true;
final _authService = getIt<AuthService>();
final allowedLoggedOut = [
  ScreenPaths.splash,
  ScreenPaths.signin,
  ScreenPaths.signup
];

final appRouter = GoRouter(
    initialLocation: ScreenPaths.timelines,
    debugLogDiagnostics: true,
    refreshListenable: _authService,
    redirect: (context, state) async {
      final loggedIn = _authService.loggedIn;
      if (!loggedIn && !allowedLoggedOut.contains(state.location)) {
        return ScreenPaths.signin;
      }

      if (loggedIn && allowedLoggedOut.contains(state.location)) {
        return ScreenPaths.timelines;
      }

      return null;
    },
    routes: [
      GoRoute(
        path: ScreenPaths.signin,
        name: ScreenPaths.signin,
        builder: (context, state) => SignInScreen(),
      ),
      GoRoute(
        path: ScreenPaths.contacts,
        name: ScreenPaths.contacts,
        pageBuilder: (context, state) => NoTransitionPage(
          name: ScreenPaths.contacts,
          child: ContactsScreen(),
        ),
      ),
      GoRoute(
        path: '/connect/:id',
        name: ScreenPaths.connectHandle,
        builder: (context, state) =>
            FollowRequestAdjudicationScreen(userId: state.params['id']!),
      ),
      GoRoute(
        path: ScreenPaths.timelines,
        name: ScreenPaths.timelines,
        pageBuilder: (context, state) => NoTransitionPage(
          name: ScreenPaths.timelines,
          child: HomeScreen(),
        ),
      ),
      GoRoute(
        path: ScreenPaths.profile,
        name: ScreenPaths.profile,
        pageBuilder: (context, state) => NoTransitionPage(
          child: ProfileScreen(),
        ),
      ),
      GoRoute(
        path: ScreenPaths.menu,
        name: ScreenPaths.menu,
        pageBuilder: (context, state) => NoTransitionPage(
          child: MenusScreen(),
        ),
      ),
      GoRoute(
        path: ScreenPaths.messages,
        name: ScreenPaths.messages,
        pageBuilder: (context, state) => NoTransitionPage(
          child: MessagesScreen(),
        ),
        routes: [
          GoRoute(
            path: 'new_thread',
            pageBuilder: (context, state) => NoTransitionPage(
              child: MessagesNewThread(),
            ),
          ),
        ],
      ),
      GoRoute(
        name: ScreenPaths.thread,
        path: ScreenPaths.thread,
        builder: (context, state) =>
            MessageThreadScreen(parentThreadId: state.queryParams['uri']!),
      ),
      GoRoute(
        path: ScreenPaths.settings,
        name: ScreenPaths.settings,
        pageBuilder: (context, state) => NoTransitionPage(
          child: SettingsScreen(),
        ),
      ),
      GoRoute(
        path: ScreenPaths.gallery,
        name: ScreenPaths.gallery,
        pageBuilder: (context, state) => NoTransitionPage(
          child: GalleryBrowsersScreen(),
        ),
        routes: [
          GoRoute(
            path: 'show/:name',
            builder: (context, state) => GalleryScreen(
              galleryName: state.params['name']!,
            ),
          ),
        ],
      ),
      GoRoute(
        path: ScreenPaths.notifications,
        name: ScreenPaths.notifications,
        pageBuilder: (context, state) => NoTransitionPage(
          child: NotificationsScreen(),
        ),
      ),
      GoRoute(
        path: ScreenPaths.splash,
        name: ScreenPaths.splash,
        builder: (context, state) => SplashScreen(),
      ),
      GoRoute(
          path: '/post',
          redirect: (context, state) {
            if (state.location == '/post') {
              return '/post/new';
            }

            return null;
          },
          routes: [
            GoRoute(
              path: 'new',
              builder: (context, state) => EditorScreen(),
            ),
            GoRoute(
              path: 'edit/:id',
              builder: (context, state) =>
                  EditorScreen(id: state.params['id'] ?? 'Not Found'),
            ),
            GoRoute(
              path: 'view/:id/:goto_id',
              builder: (context, state) => PostScreen(
                id: state.params['id'] ?? 'Not Found',
                goToId: state.params['goto_id'] ?? 'Not Found',
              ),
            ),
          ]),
      GoRoute(
          path: '/comment',
          redirect: (context, state) {
            if (state.location == '/comment') {
              return '/comment/new';
            }

            return null;
          },
          routes: [
            GoRoute(
              path: 'new',
              builder: (context, state) => EditorScreen(
                parentId: state.queryParams['parent_id'] ?? '',
              ),
            ),
            GoRoute(
              path: 'edit/:id',
              builder: (context, state) =>
                  EditorScreen(id: state.params['id'] ?? 'Not Found'),
            ),
          ]),
      GoRoute(
        path: '/user_posts/:id',
        name: ScreenPaths.userPosts,
        builder: (context, state) =>
            UserPostsScreen(userId: state.params['id']!),
      ),
      GoRoute(
        path: '/user_profile/:id',
        name: ScreenPaths.userProfile,
        builder: (context, state) =>
            UserProfileScreen(userId: state.params['id']!),
      ),
    ]);
