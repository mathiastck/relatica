import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../controls/app_bottom_nav_bar.dart';
import '../controls/notifications_control.dart';
import '../globals.dart';
import '../services/notifications_manager.dart';
import '../utils/snackbar_builder.dart';

class NotificationsScreen extends StatelessWidget {
  static final _logger = Logger('$NotificationsScreen');

  const NotificationsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    _logger.finest('Building');
    final manager = context.watch<NotificationsManager>();
    final notifications = manager.notifications;
    late final String title;
    late final Widget body;
    if (notifications.isEmpty) {
      manager.updateNotifications();
      title = 'Notifications';
      body = Center(
          child: ListView(
        children: [
          Center(child: Text('No notifications')),
        ],
      ));
    } else {
      final unreadCount = notifications.where((e) => !e.dismissed).length;
      title = 'Notifications ($unreadCount)';
      body = ListView.separated(
          itemBuilder: (context, index) {
            return NotificationControl(notification: notifications[index]);
          },
          separatorBuilder: (context, index) {
            return Divider(
              color: Colors.black54,
              height: 0.0,
            );
          },
          itemCount: notifications.length);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: [
          IconButton(
            onPressed: () async => _clearAllNotifications(context, manager),
            icon: Icon(Icons.clear_all),
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await manager.updateNotifications();
        },
        child: body,
      ),
      bottomNavigationBar: AppBottomNavBar(
        currentButton: NavBarButtons.notifications,
      ),
    );
  }

  Future<void> _clearAllNotifications(
      BuildContext context, NotificationsManager manager) async {
    final confirmed =
        await showYesNoDialog(context, 'Clear all notifications?');
    if (confirmed == true) {
      final message = (await manager.markAllAsRead()).fold(
          onSuccess: (_) => 'Marked all notifications as read',
          onError: (error) => 'Error marking notifications: $error');
      buildSnackbar(context, message);
    }
  }
}
