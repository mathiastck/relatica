import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import '../controls/padding.dart';
import '../globals.dart';
import '../models/credentials.dart';
import '../services/auth_service.dart';
import '../services/secrets_service.dart';
import '../utils/snackbar_builder.dart';

class SignInScreen extends StatefulWidget {
  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  var hidePassword = true;

  @override
  void initState() {
    super.initState();
    getIt<SecretsService>().credentials.andThenSuccess((credentials) {
      if (credentials.isEmpty) {
        return;
      }

      usernameController.text = credentials.handle;
      passwordController.text = credentials.password;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign In'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: formKey,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: usernameController,
                  validator: (value) => EmailValidator.validate(value ?? '')
                      ? null
                      : 'Not a valid Friendica Account Address',
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.alternate_email),
                    hintText: 'Username (user@example.com)',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Theme.of(context).backgroundColor,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    labelText: 'Username',
                  ),
                ),
                const VerticalPadding(),
                TextFormField(
                  obscureText: hidePassword,
                  controller: passwordController,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.password),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          hidePassword = !hidePassword;
                        });
                      },
                      icon: hidePassword
                          ? const Icon(Icons.remove_red_eye_outlined)
                          : const Icon(Icons.remove_red_eye),
                    ),
                    hintText: 'Password',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Theme.of(context).backgroundColor,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    labelText: 'Password',
                  ),
                ),
                const VerticalPadding(),
                ElevatedButton(
                  onPressed: () => _signIn(context),
                  child: const Text('Signin'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _signIn(BuildContext context) async {
    if (formKey.currentState?.validate() ?? false) {
      final result = await Credentials.buildFromHandle(
        usernameController.text,
        passwordController.text,
      ).andThenAsync((creds) async {
        return await getIt<AuthService>().signIn(creds);
      });

      if (result.isFailure) {
        buildSnackbar(context, 'Error signing in: ${result.error}');
      }
    }
  }
}
