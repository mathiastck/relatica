import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../controls/timeline/post_control.dart';
import '../services/timeline_manager.dart';

class PostScreen extends StatelessWidget {
  final String id;

  final String goToId;

  const PostScreen({
    super.key,
    required this.id,
    required this.goToId,
  });

  @override
  Widget build(BuildContext context) {
    final manager = context.watch<TimelineManager>();
    final body = manager.getPostTreeEntryBy(id).fold(
        onSuccess: (post) => RefreshIndicator(
              onRefresh: () async {
                await manager.refreshStatusChain(id);
              },
              child: PostControl(
                originalItem: post,
                scrollToId: goToId,
                openRemote: true,
                showStatusOpenButton: true,
                isRoot: true,
              ),
            ),
        onError: (error) => Text('Error getting post: $error'));
    return Scaffold(
        appBar: AppBar(
          title: Text('View Post'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: body,
        ));
  }
}
