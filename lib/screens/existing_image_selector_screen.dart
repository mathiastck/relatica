import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/gallery_data.dart';
import '../models/image_entry.dart';
import '../serializers/friendica/image_entry_friendica_extensions.dart';
import '../services/gallery_service.dart';
import 'image_viewer_screen.dart';

class ExistingImageSelectorScreen extends StatefulWidget {
  @override
  State<ExistingImageSelectorScreen> createState() =>
      _ExistingImageSelectorScreenState();
}

class _ExistingImageSelectorScreenState
    extends State<ExistingImageSelectorScreen> {
  GalleryData? selectedGallery;
  final selectedImages = <ImageEntry>[];

  @override
  Widget build(BuildContext context) {
    final service = context.watch<GalleryService>();
    final title = selectedImages.isEmpty
        ? 'Select Image(s)'
        : '${selectedImages.length} selected';
    final galleries = service.getGalleries();
    final List<ImageEntry> images = selectedGallery == null
        ? []
        : service
            .getGalleryImageList(selectedGallery!.name)
            .getValueOrElse(() => []);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
          actions: [
            IconButton(
              onPressed: selectedImages.isEmpty
                  ? null
                  : () {
                      Navigator.of(context).pop(selectedImages);
                    },
              tooltip: 'Attach selected files',
              icon: Icon(Icons.attach_file),
            ),
            IconButton(
              onPressed: selectedImages.isEmpty
                  ? null
                  : () {
                      setState(() {
                        selectedImages.clear();
                      });
                    },
              tooltip: 'Clear Selection',
              icon: Icon(Icons.remove_circle_outline),
            ),
          ],
        ),
        body: Center(
            child: Column(
          children: [
            DropdownButton<GalleryData>(
                value: selectedGallery,
                items: galleries
                    .map((g) => DropdownMenuItem(value: g, child: Text(g.name)))
                    .toList(),
                onChanged: (value) {
                  setState(() {
                    selectedGallery = value;
                  });
                }),
            const VerticalDivider(),
            Expanded(child: buildGrid(context, images)),
          ],
        )),
      ),
    );
  }

  Widget buildGrid(BuildContext context, List<ImageEntry> images) {
    const thumbnailDimension = 350.0;
    return GridView.builder(
        itemCount: images.length,
        padding: const EdgeInsets.all(5.0),
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: thumbnailDimension),
        itemBuilder: (context, index) {
          final image = images[index];
          final imageWidget = CachedNetworkImage(
            imageUrl: image.thumbnailUrl,
          );
          final selected = selectedImages.contains(image);

          final tileWidget = selected
              ? Stack(
                  children: [
                    imageWidget,
                    Positioned(
                        child: Icon(
                      Icons.check_circle,
                      color: Colors.green,
                    ))
                  ],
                )
              : imageWidget;
          return Padding(
            padding: const EdgeInsets.all(2.0),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  if (selected) {
                    selectedImages.remove(image);
                  } else {
                    selectedImages.add(image);
                  }
                });
              },
              onDoubleTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ImageViewerScreen(
                      attachment: image.toMediaAttachment(),
                    ),
                  ),
                );
              },
              child: tileWidget,
            ),
          );
        });
  }
}
